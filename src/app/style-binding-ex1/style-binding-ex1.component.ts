import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-style-binding-ex1',
  templateUrl: './style-binding-ex1.component.html',
  styleUrls: ['./style-binding-ex1.component.css'],
  
})
export class StyleBindingEx1Component implements OnInit {

  constructor() { }
  sample="sample sample1";
  ngOnInit() {
  }

}
