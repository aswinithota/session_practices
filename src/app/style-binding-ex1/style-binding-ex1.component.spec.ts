import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StyleBindingEx1Component } from './style-binding-ex1.component';

describe('StyleBindingEx1Component', () => {
  let component: StyleBindingEx1Component;
  let fixture: ComponentFixture<StyleBindingEx1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StyleBindingEx1Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StyleBindingEx1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
