import { Component, OnInit,Input } from '@angular/core';

@Component({
  selector: 'app-student-list-child',
  templateUrl: './student-list-child.component.html',
  styleUrls: ['./student-list-child.component.css']
})
export class StudentListChildComponent implements OnInit {
  @Input()
  empParentData:string="";
  
  data:any;
  constructor() { 
    this.data = this.empParentData; 
  }

  ngOnInit() {
  }
  

}
