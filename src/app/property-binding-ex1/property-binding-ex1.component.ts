import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-property-binding-ex1',
  templateUrl: './property-binding-ex1.component.html',
  styleUrls: ['./property-binding-ex1.component.css']
})
export class PropertyBindingEx1Component implements OnInit {

  constructor() { }

  ngOnInit() {
  }
  path1:string="./assets/puppy.jpg";
  title1:string='Micky';

}
