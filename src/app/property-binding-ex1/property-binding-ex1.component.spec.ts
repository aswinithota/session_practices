import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PropertyBindingEx1Component } from './property-binding-ex1.component';

describe('PropertyBindingEx1Component', () => {
  let component: PropertyBindingEx1Component;
  let fixture: ComponentFixture<PropertyBindingEx1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PropertyBindingEx1Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PropertyBindingEx1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
