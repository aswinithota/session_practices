import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http'
@Injectable({
  providedIn: 'root'
})
export class JokeService {
url:string="http://api.icndb.com/jokes/random";
data:any={}
  constructor(private _http:HttpClient) {
   
   }
   getJoke(){
   this._http.get(this.url)
   .subscribe(response => this.data = response)
   
   return this.data;
  }
}
