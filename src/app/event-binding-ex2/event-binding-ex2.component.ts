import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-event-binding-ex2',
  templateUrl: './event-binding-ex2.component.html',
  styleUrls: ['./event-binding-ex2.component.css']
})
export class EventBindingEx2Component implements OnInit {

  constructor() { }

  ngOnInit() {
  }
   name:string="";
   msg:string="";
  displayName(){
    this.msg = "Hello "+this.name +" :)";
  }
  clearName(){
    this.msg = "";
    this.name = "";
  }
}
