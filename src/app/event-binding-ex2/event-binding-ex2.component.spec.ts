import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EventBindingEx2Component } from './event-binding-ex2.component';

describe('EventBindingEx2Component', () => {
  let component: EventBindingEx2Component;
  let fixture: ComponentFixture<EventBindingEx2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EventBindingEx2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventBindingEx2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
