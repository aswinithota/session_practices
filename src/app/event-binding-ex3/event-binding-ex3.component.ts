import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-event-binding-ex3',
  templateUrl: './event-binding-ex3.component.html',
  styleUrls: ['./event-binding-ex3.component.css']
})
export class EventBindingEx3Component implements OnInit {

  constructor() { }
a:number;
b:number;
msg:string;
  ngOnInit() {
  }
  Calculate(){
    this.msg=`Add of ${this.a} and ${this.b} is ${this.a +this.b} `;
  }

}
