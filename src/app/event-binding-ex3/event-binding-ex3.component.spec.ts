import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EventBindingEx3Component } from './event-binding-ex3.component';

describe('EventBindingEx3Component', () => {
  let component: EventBindingEx3Component;
  let fixture: ComponentFixture<EventBindingEx3Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EventBindingEx3Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventBindingEx3Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
