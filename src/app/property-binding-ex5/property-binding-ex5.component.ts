import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-property-binding-ex5',
  templateUrl: './property-binding-ex5.component.html',
  styleUrls: ['./property-binding-ex5.component.css']
})
export class PropertyBindingEx5Component implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  path1:string="";
  title1:string='';
  getImage(p){
    if(p=="Puppy"){
      this.path1 = "./assets/puppy.jpg";
      this.title1 = "Pomerian";
    }else if(p == ""){
      this.path1 = "";
      this.title1 = "";
    }
  }
}
