import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PropertyBindingEx5Component } from './property-binding-ex5.component';

describe('PropertyBindingEx5Component', () => {
  let component: PropertyBindingEx5Component;
  let fixture: ComponentFixture<PropertyBindingEx5Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PropertyBindingEx5Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PropertyBindingEx5Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
