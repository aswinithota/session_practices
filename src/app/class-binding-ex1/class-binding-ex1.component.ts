import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-class-binding-ex1',
  templateUrl: './class-binding-ex1.component.html',
  styleUrls: ['./class-binding-ex1.component.css']
})
export class ClassBindingEx1Component implements OnInit {

  constructor() { }

  ngOnInit() {
  }
  style = {'color':'orange'}
  style1 ={'color':'black'}
  style2 ={'color':'black'}
  success(){
    this.style={'color':'green'}
    this.style1 = {'color':'blue'}
  }
  failure(){
    this.style={'color':'red'}
    this.style2 = {'color':'pink'}
  }
}
