import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClassBindingEx1Component } from './class-binding-ex1.component';

describe('ClassBindingEx1Component', () => {
  let component: ClassBindingEx1Component;
  let fixture: ComponentFixture<ClassBindingEx1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClassBindingEx1Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClassBindingEx1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
