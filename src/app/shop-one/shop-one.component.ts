import { Component, OnInit } from '@angular/core';
import { BakeryService } from '../bakery.service'
@Component({
  selector: 'app-shop-one',
  templateUrl: './shop-one.component.html',
  styleUrls: ['./shop-one.component.css'],
  providers:[BakeryService]
})
export class ShopOneComponent implements OnInit {
 biscuits:string[];
  constructor(private _biscuit:BakeryService) { }

  ngOnInit() {
  }

 getItem(){
   this.biscuits = this._biscuit.chocochips;
 }
}
