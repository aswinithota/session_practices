import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class BakeryService {
  chocochips:string[];
  constructor() { 
    this.chocochips = ['magic','mom']
  }
}
