import { Component, OnInit, Input,Output ,EventEmitter} from '@angular/core';

@Component({
  selector: 'app-child',
  templateUrl: './child.component.html',
  styleUrls: ['./child.component.css']
})
export class ChildComponent implements OnInit {
 @Input()
 parentData:string="";
 
 @Output()
 childEvent:any = new EventEmitter<string>();
 t1:string="";
  constructor() { }
msg:string='';
display(){
  this.msg = this.parentData;
}
sendDataToParent(t1){
  this.childEvent.emit(t1);
}
  ngOnInit() {
  }

}
