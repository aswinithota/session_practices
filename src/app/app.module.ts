import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { EventBindingEx1Component } from './event-binding-ex1/event-binding-ex1.component';
import { EventBindingEx2Component } from './event-binding-ex2/event-binding-ex2.component';
import { EventBindingEx3Component } from './event-binding-ex3/event-binding-ex3.component';
import { EventBindingEx4Component } from './event-binding-ex4/event-binding-ex4.component';
import { EventBindingEx5Component } from './event-binding-ex5/event-binding-ex5.component';
import { PropertyBindingEx5Component } from './property-binding-ex5/property-binding-ex5.component';
import { PropertyBindingEx1Component } from './property-binding-ex1/property-binding-ex1.component';
import { StyleBindingEx1Component } from './style-binding-ex1/style-binding-ex1.component';
import { ClassBindingEx1Component } from './class-binding-ex1/class-binding-ex1.component';
import { DirectivesComponent } from './directives/directives.component';
import { ParentComponent } from './parent/parent.component';
import { ChildComponent } from './child/child.component';
import { StudentListParentComponent } from './student-list-parent/student-list-parent.component';
import { StudentListChildComponent } from './student-list-child/student-list-child.component';
import { DependencyInjectionComponent } from './dependency-injection/dependency-injection.component';
import { ShopOneComponent } from './shop-one/shop-one.component';
import { ShopTwoComponent } from './shop-two/shop-two.component';
import { ProductsComponent } from './products/products.component';
import { MyPipePipe } from './my-pipe.pipe';
import { FormsComponent } from './forms/forms.component';
import { HttpClientModule } from '@angular/common/http';
import { ServiceEg1Component } from './service-eg1/service-eg1.component';
import { ServiceEg2Component } from './service-eg2/service-eg2.component';
import { AboutComponent } from './about/about.component';
import { LoginComponent } from './login/login.component';
import { RouterModule } from '@angular/router'

@NgModule({
  declarations: [
    AppComponent,
    EventBindingEx1Component,
    EventBindingEx2Component,
    EventBindingEx3Component,
    EventBindingEx4Component,
    EventBindingEx5Component,
    PropertyBindingEx5Component,
    PropertyBindingEx1Component,
    StyleBindingEx1Component,
    ClassBindingEx1Component,
    DirectivesComponent,
    ParentComponent,
    ChildComponent,
    StudentListParentComponent,
    StudentListChildComponent,
    DependencyInjectionComponent,
    ShopOneComponent,
    ShopTwoComponent,
    ProductsComponent,
    MyPipePipe,
    FormsComponent,
    ServiceEg1Component,
    ServiceEg2Component,
    AboutComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    RouterModule.forRoot(
      [
        {path:'login',component:LoginComponent},
        {path:'about',component:AboutComponent},
      
        {path:'about',component:AboutComponent},
        {path:'',redirectTo:'/about',pathMatch:'full'},
        {path:'**',component:FormsComponent}
      ]
    )
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
