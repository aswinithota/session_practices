import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-service-eg2',
  templateUrl: './service-eg2.component.html',
  styleUrls: ['./service-eg2.component.css']
})
export class ServiceEg2Component implements OnInit {

  constructor(private _http:HttpClient) { }

  ngOnInit() {
  }
url:string="http://localhost:3000/students";
data:Student[];

getStudentfromApi(){
  this._http.get<Student[]>(this.url)
  .subscribe(response => this.data = response);
}
 

}



class Student{
  id:number;
  name:string;
  course:string;
}