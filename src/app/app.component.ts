import { Component } from '@angular/core';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Angular';
  name = 'Micky Thota';
  direction = "West";
  msg = '';
  items = ["east","west","north","south"];
  num = 2;
  butonName  = "click Here";
  get(p){
    let d = new Date();
    if(p == 'date'){
      return d.getDate()+"/"+(d.getMonth()+1)+"/"+d.getFullYear();
    }
    else if(p === 'time'){
      return d.getHours()+":"+d.getMinutes()+":"+d.getSeconds();
    }
  }
 
}
 