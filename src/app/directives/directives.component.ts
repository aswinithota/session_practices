import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-directives',
  templateUrl: './directives.component.html',
  styleUrls: ['./directives.component.css']
})
export class DirectivesComponent implements OnInit {
  employees:any[];
  x={};
  constructor() {
     this.fruits = ["Apple","Mango","sapota","qiwi","custard apple"];
    this.employees = [
      {id:1,ename:'Ashu',job:'Admin',sal:'50000'},
      {id:2,ename:'Jon',job:'Programmer',sal:'30000'},
      {id:3,ename:'Jack',job:'Developer',sal:'10000'},
      {id:4,ename:'Mic',job:'Trainer',sal:'20000'}
    ];
   }

  ngOnInit() {
  }
  
  fruits = [];
  heading:string;
  msg:string;
  items:string[];
  vegetables = ["potato","brinjal","beans","peas"]

  display(item){
    if(item == "fruits"){
      this.heading = "List of Fruits";
      this.items = this.fruits;
    }else if(item == "vegetables"){
      this.heading = "List of vegetables";
      this.items = this.vegetables;
    }
    this.msg = `Count of item ${item} are ${this.items}`
  }

  search(i){
    this.x = this.employees[i];
  }

  delete(i){
    if(confirm("Are you sure")){
      this.employees.splice(i,1);
      this.x ={};
    }
  }
  add(){
    if(!(this.x)){
      this.employees.push(this.x);
    }
    else{
      alert("please enter data")
    }
  }
}
