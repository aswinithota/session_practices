import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-event-binding-ex5',
  templateUrl: './event-binding-ex5.component.html',
  styleUrls: ['./event-binding-ex5.component.css']
})
export class EventBindingEx5Component implements OnInit {

  constructor() { }

  ngOnInit() {
  }
  name:string="";
  terms:boolean;
  msg="";

  display(){
    if(this.terms){
      this.msg=`Thank You`;
    }
    else{
      this.msg=`Please accept the terms`;
    }
  }
}
