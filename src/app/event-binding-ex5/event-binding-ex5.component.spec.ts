import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EventBindingEx5Component } from './event-binding-ex5.component';

describe('EventBindingEx5Component', () => {
  let component: EventBindingEx5Component;
  let fixture: ComponentFixture<EventBindingEx5Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EventBindingEx5Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventBindingEx5Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
