import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EventBindingEx1Component } from './event-binding-ex1.component';

describe('EventBindingEx1Component', () => {
  let component: EventBindingEx1Component;
  let fixture: ComponentFixture<EventBindingEx1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EventBindingEx1Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventBindingEx1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
