import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-event-binding-ex1',
  templateUrl: './event-binding-ex1.component.html',
  styleUrls: ['./event-binding-ex1.component.css']
})
export class EventBindingEx1Component implements OnInit {
  msg:string = "";
  constructor() { }
  sayHello(){
    this.msg = "Micky";
  }
  ngOnInit() {
  }

}
