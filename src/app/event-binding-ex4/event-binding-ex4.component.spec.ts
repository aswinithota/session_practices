import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EventBindingEx4Component } from './event-binding-ex4.component';

describe('EventBindingEx4Component', () => {
  let component: EventBindingEx4Component;
  let fixture: ComponentFixture<EventBindingEx4Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EventBindingEx4Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventBindingEx4Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
