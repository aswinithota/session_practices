import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-event-binding-ex4',
  templateUrl: './event-binding-ex4.component.html',
  styleUrls: ['./event-binding-ex4.component.css']
})
export class EventBindingEx4Component implements OnInit {

  constructor() { }
  msg:string="";
  gender:string="";
  name:string="";
  ngOnInit() {
  }

  display(){
    this.msg = `${this.gender}+${this.name}`
  }

}
