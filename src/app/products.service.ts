import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ProductsService {
product:any[];
  constructor() {
    this.product = [
      {id:1,name:"Tv",brand:"philips",cost:'12000'},
      {id:2,name:"Mobile",brand:"Mi",cost:'12300'},
      {id:3,name:"Mobile",brand:"Iphone",cost:'12000'}
    ]
   }
}
