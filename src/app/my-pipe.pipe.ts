import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'myPipe'
})
export class MyPipePipe implements PipeTransform {

  transform(value: any, args?: any): any {
    // return null;
    if(value == 'ashwini'){
      return "Hello"+value;
    }else{
      return "Hi"+value;
    }
  }

}
