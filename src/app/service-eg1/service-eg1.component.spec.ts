import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ServiceEg1Component } from './service-eg1.component';

describe('ServiceEg1Component', () => {
  let component: ServiceEg1Component;
  let fixture: ComponentFixture<ServiceEg1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ServiceEg1Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ServiceEg1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
