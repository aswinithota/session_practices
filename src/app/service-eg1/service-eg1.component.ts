import { Component, OnInit } from '@angular/core';
import { JokeService} from  '../joke.service'

@Component({
  selector: 'app-service-eg1',
  templateUrl: './service-eg1.component.html',
  styleUrls: ['./service-eg1.component.css'],
  providers:[JokeService]
})
export class ServiceEg1Component implements OnInit {

  constructor(private _dataFromService:JokeService) { }

  ngOnInit() {
  }

  data:any={};
  getData(){
    this.data = this._dataFromService.getJoke();
  }

}
